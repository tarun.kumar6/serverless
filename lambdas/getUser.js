const Responses = require('./API_Responses');

const data = {
    1234: {
        name: 'Arjit Bhandari',
        age: 22,
        job:'developer'
    },
    7893: {
        name: 'Akash Joshi',
        age: 40,
        job:'teacher'
    },
    3986: {
        name: 'Siddharth Sinha',
        age: 30,
        job:'Enginner'
    }
};

exports.handler = async event => {
    console.log('event', event);

    if (!event.pathParameters || !event.pathParameters.ID) {
        // failed without an ID
        return Responses._400({ message: 'missing the ID from the path' });
    }

    let ID = event.pathParameters.ID;

    if (data[ID]) {
        // return the data
        return Responses._200(data[ID]);
    }

    //failed as ID not in the data
    return Responses._400({ message: 'no ID in data' });
};